package com.stock
{
	import flash.events.Event;
	
	public class StockEvent extends Event
	{
		/**
		 * 주 코드가 변경됨 
		 */		
		public static const STOCK_CODE_CHANGED:String = "stockCodeChanged";
		
		/**
		 * 주가 정보가 변경됨 
		 */		
		public static const STOCK_CHANGED:String = "stockChanged";
		
		private var mData:*;
		
		public function StockEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public function set data( value:* ) : void { 
			this.mData = value;
		}
		
		public function get data() : * { 
			return mData;
		}
	}
}