package com.stock.model
{
	public interface IMCache
	{
		function get( key:String ) : *;
		function set( key:String, value:* ) : void;
	}
}