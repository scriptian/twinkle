package com.stock.model
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import logging.StockLog;

	public class MFileCache implements IMCache
	{
		private const CACHE_FILE_NAME:String = "twinkle.ini";
		
		private static var mInstance:MFileCache;
		
		private var mCache:FileStream = new FileStream();
		
		private var mCacheFile:File;
		
		private var mCachedInfo:Object;
		
		public function MFileCache()
		{
			
		}
		
		public static function get instance() : MFileCache
		{
			if( null == mInstance )
				mInstance = new MFileCache();
			
			return mInstance; 
		}
		
		public function get(key:String):*
		{
			StockLog.d( "MFileCache.get key:"+key );
			
			var cachedInfo:Object = getCachedInfo();
			
			return cachedInfo[ key ];
		}
		
		public function set( key:String, value:* ) : void
		{
			StockLog.d( "MFileCache.set key:"+key );
			
			var cachedInfo:Object = getCachedInfo();
			
			cachedInfo[ key ] = value;
			
			updateCachedInfo( cachedInfo );
		}
		
		private function getCachedInfo() : Object
		{
			if( null == mCachedInfo )
			{
				var filePath:String = File.applicationDirectory.nativePath+File.separator+CACHE_FILE_NAME;
				StockLog.d( "MFileCache.getCachedInfo filePath:"+filePath );
				mCacheFile = File.applicationDirectory.resolvePath(filePath);
				
				if( !mCacheFile.exists ) { 
					
					var tmFile:File = File.createTempFile();
					tmFile.copyTo( mCacheFile, true );
				}
				
				mCache.open(mCacheFile, FileMode.READ);
				
				var cachedRawInfo:String = mCache.readUTFBytes( mCache.bytesAvailable );
				
				mCache.close();
				
				if( null != cachedRawInfo && cachedRawInfo.length > 0 ) 
					mCachedInfo = JSON.parse( cachedRawInfo );
				else
					mCachedInfo = {};
			}
			
			return mCachedInfo;
		}
		
		private function updateCachedInfo( newInfo:Object ) : void
		{
			mCache.open(mCacheFile, FileMode.WRITE);
			
			mCache.writeUTFBytes( JSON.stringify( newInfo ) );
			
			mCache.close();
		}
	}
}