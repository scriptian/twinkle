package com.stock.model.vo
{
	/**
	 * 네이버 주가 정보 
	 * @author kanchi
	 * 
	 */	
	public class StockNaver extends Stock
	{
		private var mRawData:Object;
		
		public function StockNaver( data:Object )
		{
			mRawData = data;
		}
		
		override public function get direction() : uint 
		{ 
			return stockInfo.rf;	
		}
		
		override public function get changeRate() : Number
		{
			return stockInfo.cr;
		}
		
		override public function get changeValue() : Number
		{
			return stockInfo.cv;
		}
		
		override public function get preCloseValue() : Number
		{
			return stockInfo.pcv;
		}
		
		override public function get nowValue() : Number 
		{
			return stockInfo.nv;
		}
		
		override public function get marketStatus() : String
		{
			return stockInfo.ms;
		}
		
		override public function get name() : String
		{
			return stockInfo.nm;
		}
		
		override public function get code() : String
		{
			return stockInfo.cd;
		}
		
		private function get stockInfo() : Object
		{
			return mRawData.result.areas[0].datas[0];
		}
	}
}