package com.stock.model.vo
{
	public interface IStock
	{
		/**
		 * 방향성 ( 상승 / 하락 ) 
		 * 상승 : Stock.DIRECT_UP
		 * 하락 : Stock.DIRECT_DOWN 
		 * @return 
		 * 
		 */		
		function get direction() : uint;
		
		/**
		 * 변동률  
		 * @return 
		 * 
		 */		
		function get changeRate() : Number;
		
		/**
		 * 변동값 
		 * @return 
		 * 
		 */		
		function get changeValue() : Number;
		
		/**
		 * 전날 장 마감 가격 
		 * @return 
		 * 
		 */		
		function get preCloseValue() : Number;
		
		/**
		 * 현재 시장가 
		 * @return 
		 * 
		 */		
		function get nowValue() : Number;
		
		/**
		 * 장 상태 
		 * @return 
		 * 
		 */		
		function get marketStatus() : String;
		
		/**
		 * 종목명  
		 * @return 
		 * 
		 */		
		function get name() : String;
		
		/**
		 * 주가 코드번호
		 * @return 
		 * 
		 */
		function get code() : String;
	}
}