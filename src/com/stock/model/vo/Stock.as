package com.stock.model.vo
{
	public class Stock implements IStock
	{
		/**
		 * 마켓 상태 
		 */		
		public static const MARKET_STAT_OPEN:String = "OPEN";
		public static const MARKET_STAT_CLOSE:String = "CLOSE";
		
		/**
		 * 방향성 ( 상승, 하락 ) 
		 */		
		public static const DIRECT_UP:uint = 2;
		public static const DIRECT_DOWN:uint = 5;
		
		public function Stock()
		{
		}
		
		public function get direction() : uint 
		{ 
			return 0;
		}
		
		public function get changeRate() : Number
		{
			return 0;
		}
		
		public function get changeValue() : Number
		{
			return 0;
		}
		
		public function get preCloseValue() : Number
		{
			return 0;
		}
		
		public function get nowValue() : Number 
		{
			return 0;
		}
		
		public function get marketStatus() : String
		{
			return null;
		}
		
		public function get name() : String
		{
			return null;
		}
		
		public function get code() : String
		{
			return null;
		}
		
		private function get stockInfo() : Object
		{
			return null;
		}
	}
}