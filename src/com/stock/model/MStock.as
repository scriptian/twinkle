package com.stock.model
{
	import com.stock.model.vo.IStock;
	import com.stock.StockEvent;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class MStock extends EventDispatcher
	{
		private static var mInstance:MStock;
		
		private var mStock:IStock;
		
		public function MStock(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		static public function instance() : MStock
		{
			if( null == mInstance ) 
				mInstance = new MStock();
			
			return mInstance;
		}
		
		public function reset() : void 
		{
			this.mStock = null;
		}
		
		[Bindable(event="stockChanged")] 
		public function set stock( value:IStock ) : void 
		{
			this.mStock = value;
			
			var newEvent:StockEvent = new StockEvent( StockEvent.STOCK_CHANGED );
			newEvent.data = value;
			
			this.dispatchEvent( newEvent );
		}
		
		public function get stock() : IStock
		{
			return mStock;
		}
	}
}