package com.stock.contoller
{
	public interface ICStock
	{
		/**
		 * 현재 조회된 주가정보 초기화 
		 * 
		 */		
		function reset() : void;
		
		/**
		 * 주가 정보 조회 시작
		 * @param stockCode 주가 코드 
		 * 
		 */		
		function loadStock( stockCode:String ) : void;
	}
}