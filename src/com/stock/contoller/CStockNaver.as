package com.stock.contoller
{
	import com.stock.model.IMCache;
	import com.stock.model.MFileCache;
	import com.stock.model.MStock;
	import com.stock.model.vo.Stock;
	import com.stock.model.vo.StockNaver;
	import com.stock.observer.StockAlarm;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.System;
	import flash.utils.Timer;

	public class CStockNaver implements ICStock
	{
		public static const LOAD_INTERVAL:uint = 3000;
		
		private static var mInstance:CStockNaver;
		
		private var mTimer:Timer;
		
		private var mStockCode:String;
		
		private var mQuery:String = "http://polling.finance.naver.com/api/realtime.nhn?query=MOBILE_ITEM%3A$STOCK_ID";
		
		private var mUrlLoader:URLLoader = new URLLoader();
		
		public function CStockNaver()
		{
			System.useCodePage = true;
			
			mTimer = new Timer(LOAD_INTERVAL);
			mTimer.addEventListener(TimerEvent.TIMER, onTime);
			
			mUrlLoader.addEventListener(Event.COMPLETE, onLoadComp);
		}
		
		static public function get instance() : CStockNaver
		{
			if( null == mInstance )
				mInstance = new CStockNaver();
			
			return mInstance;
		}
		
		protected function onTime(event:TimerEvent = null):void
		{
			if( null == mStockCode || mStockCode.length != 6 ) { 
				mTimer.stop();
				return;
			}
			
			var query:String = mQuery.replace("$STOCK_ID",mStockCode);
			var urlReq:URLRequest = new URLRequest(query);
			
			mUrlLoader.load(urlReq);
		}
		
		protected function onLoadComp(event:Event):void
		{
			var result:Object = JSON.parse( event.currentTarget.data );	
			var stock:StockNaver = new StockNaver( result );
			
			if( Stock.MARKET_STAT_CLOSE == stock.marketStatus ) {
				mTimer.stop();
			}
			
			MStock.instance().stock = stock;
		}
		
		public function reset() : void 
		{
			MStock.instance().reset();
		}
		
		public function loadCachedInfo() : void
		{
			var cachedStockCode:String =  cache.get("currentStockCode");
			
			if( null != cachedStockCode && cachedStockCode.length > 0 )
			{
				loadStock( cachedStockCode );
			}
		}
		
		public function loadStock( stockCode:String ) : void 
		{
			reset();
			
			mStockCode = stockCode;
			
			cache.set("currentStockCode",stockCode);
			
			StockAlarm.instance().loadCachedInfo( stockCode );
			
			mTimer.start();
			
			onTime();
		}
		
		private function get cache() : IMCache
		{
			return MFileCache.instance;
		}
	}
}