package com.stock.observer
{
	import flash.events.Event;
	
	public class StockAlarmEvent extends Event
	{
		public static const ALARM_CHANGED:String = "stockAlaramChanged";
		
		public function StockAlarmEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}