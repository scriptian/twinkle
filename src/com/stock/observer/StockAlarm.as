package com.stock.observer
{
	import com.stock.model.MFileCache;
	import com.stock.model.vo.IStock;
	
	import flash.events.EventDispatcher;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	import mx.collections.ArrayCollection;
	
	import logging.StockLog;

	public class StockAlarm extends EventDispatcher
	{
		private static var mInstance:StockAlarm;
		
		private var mStockCode:String;
		
		private var mAlaramInfo:Object = {};
		
		public function StockAlarm()
		{
			
		}
		
		static public function instance() : StockAlarm
		{
			if( null == mInstance )
				mInstance = new StockAlarm();
			
			return mInstance;
		}
		
		public function loadCachedInfo( stockCode:String ) : void {
			
			if( stockCode != mStockCode || null == mAlaramInfo ) {
				
				StockLog.d( "StockAlarm.loadCachedInfo stockCode:"+stockCode );
				
				mStockCode = stockCode; 
				
				var cachedAlaramInfo:Object = MFileCache.instance.get("alram_"+stockCode);
				
				if( null != cachedAlaramInfo ) { 
					
					var alaramList:ArrayCollection = new ArrayCollection( cachedAlaramInfo['values']);
					
					mAlaramInfo = {"pv":cachedAlaramInfo['pv'], 'values':alaramList}; 
					
				} else {
					
					mAlaramInfo = {"pv":0,"values":new ArrayCollection()};
				}
			}
		}
		
		public function saceCachedInfo() : void 
		{ 
			var value:Object = {"pv":mAlaramInfo['pv'],'values':getAlarms( mStockCode).source};
			
			MFileCache.instance.set("alram_"+mStockCode, value );
		}
		
		public function getAlarms( stockCode:String ) : ArrayCollection  
		{
			loadCachedInfo( stockCode );
			
			return mAlaramInfo.values as ArrayCollection;
		}
		
		public function addAlram( stockCode:String, value:Number ) : void 
		{
			var alarms:ArrayCollection = getAlarms( stockCode );
			
			if( alarms.getItemIndex( value ) < 0 )
			{
				alarms.addItem( value );
				
				saceCachedInfo();
			}
		}
		
		public function checkAlram( stock:IStock ) : Boolean
		{
			if( null == stock )
				return false;
			
			loadCachedInfo( stock.code );
			
			var alramInfo:Object = mAlaramInfo;
			
			if( null != alramInfo.values && ( alramInfo.values as ArrayCollection ).length > 0 ) {    
			
				var alert:Boolean = false;
				var value:Number;
				
				if( 0 == alramInfo.pv || alramInfo.pv > stock.nowValue ) 
				{	
					for each( value in alramInfo.values ) 
					{	
						if( alramInfo.pv > value &&  value >= stock.nowValue ) 
						{	
							makeSound();
							alert = true;
						}
					}
				} 
				else if( alramInfo.pv < stock.nowValue ) 
				{	
					for each( value in alramInfo.values ) 
					{	
						if( alramInfo.pv < value && value <= stock.nowValue ) 
						{	
							makeSound();
							alert = true;
						}
					}
				}
			}
			
			alramInfo.pv = stock.nowValue;
			
			this.dispatchEvent( new StockAlarmEvent( StockAlarmEvent.ALARM_CHANGED ) );
			
			return alert;
		}
		
		private function makeSound():void
		{
			var url:String = "assets/ding.mp3";
			var request:URLRequest = new URLRequest(url);
			var soundFactory:Sound = new Sound();
			soundFactory.load(request);
			soundFactory.play();
		}
		
		public function deleteAlarm(stockCode:String, value:Number):void
		{
			var alarms:ArrayCollection = this.getAlarms( stockCode );
			
			var itemIdx:uint = alarms.getItemIndex( value );
			
			alarms.removeItemAt( itemIdx );
			
			saceCachedInfo();
		}
	}
}