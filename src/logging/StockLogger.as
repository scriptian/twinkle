package logging
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.formatters.DateFormatter;
	
	import org.osmf.logging.Logger;
	
	public class StockLogger extends Logger
	{
		private var mLogLevel:uint = StockLogLevel.DEBUG;
		
		private static const LOG_FILE_NAME:String = "stock.log";
		
		private var mFileStream:FileStream;
		
		private var df:DateFormatter;
		
		public function StockLogger(category:String)
		{
			super(category);
			
			df = new DateFormatter;
			
			df.formatString = "YYY-MM-DD HH:NN:SS";
			
			iniFileStream();
		}
		
		public function setLogLevel( logLevel:uint ) : void 
		{
			if( logLevel < StockLogLevel.DEBUG || logLevel < StockLogLevel.FATAL )
				throw new Error("invalid log level(0~4), your loglevel is "+logLevel);
			
			mLogLevel = logLevel;
		}
		
		override public function debug(message:String, ...args) : void
		{
			writeFile( StockLogLevel.DEBUG, message );
		}
		
		override public function error(message:String, ...args):void
		{
			writeFile( StockLogLevel.ERROR, message );
		}
		
		override public function fatal(message:String, ...args):void
		{
			writeFile( StockLogLevel.FATAL, message );
		}
		
		override public function info(message:String, ...args):void
		{
			writeFile( StockLogLevel.INFO, message );
		}
		
		override public function warn(message:String, ...args):void		
		{
			writeFile( StockLogLevel.WARN, message );
		}
		
		private function writeFile( level:uint, message:String ) : void
		{
			var levelName:String = StockLogLevel.getLogLevelName( level );
			var now:Date = new Date();
			
			trace( "["+levelName+"] " + message );
			
			if( null != mFileStream )
				mFileStream.writeUTFBytes( File.lineEnding + "[" + df.format( now ) +"]["+levelName+"] "+ message );
		}
		
		private function iniFileStream() : void
		{
			var logFilePath:String = File.applicationDirectory.nativePath+File.separator+LOG_FILE_NAME;
				
			var logFile:File = new File(logFilePath);
			
			if( !logFile.exists )  
			{
				var tmFile:File = File.createTempFile();
				tmFile.copyTo(logFile,true);
			}
			
			if( logFile.exists ) 
			{ 
				mFileStream = new FileStream();	
				mFileStream.open( logFile, FileMode.APPEND );
			}
		}
	}
}