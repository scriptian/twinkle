package logging
{
	public class StockLogLevel
	{
		public static const DEBUG:uint 	= 0;
		public static const INFO:uint 	= 1;
		public static const WARN:uint 	= 2;
		public static const ERROR:uint 	= 3;
		public static const FATAL:uint 	= 4;
		
		public static function getLogLevelName( level:uint ) : String
		{
			switch( level ) 
			{ 
				case DEBUG 	: return "DEBUG"; break;
				case INFO 	: return "INFO"; break;
				case WARN	: return "WARN"; break;
				case ERROR 	: return "ERROR"; break;
				case FATAL 	: return "FATAL"; break;
					
			}
			
			return "UNKNOWN";
		}
	}
}