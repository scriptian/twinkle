package logging
{
	import org.osmf.logging.Logger;

	public class StockLog
	{
		private static var mLogger:Logger;
		
		public static function d( message:String,...args) : void
		{
			logger.debug( message, args );
		}
		
		public static function e( message:String, ...args) : void
		{
			logger.error( message, args );
		}
		
		public static function f( message:String, ...args) : void
		{
			logger.fatal( message, args );
		}
		
		public static function w( message:String, ...args) : void
		{
			logger.warn( message, args );
		}
		
		public static function i( message:String, ...args) : void
		{
			logger.info( message, args );
		}
		
		private static function get logger() : Logger
		{
			if( null == mLogger ) 
			{	
				mLogger = new StockLogger( "stock" );
			}
			
			return mLogger;
		}
	}
}